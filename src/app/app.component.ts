import { Component } from '@angular/core';

export class Recipe {
  id: number;
  name: string;
}

const RECIPES: Recipe[] = [
  {id: 11, name: 'Plov' },
  {id: 12, name: 'Okroshka'},
  {id: 13, name: 'Schi'},
  {id: 14, name: 'Uha'},
  {id: 15, name: 'Bliny'},
  {id: 16, name: 'Borsch'},
  {id: 17, name: 'Harcho'},
  {id: 18, name: 'Holodez'}
];

@Component({
  selector: 'app-root',
  template: `
    <h1>{{title}}</h1>
    <h2>Russian dishes</h2>
    <ul class="recipes">
      <li *ngFor="let recipe of recipes">
        <!-- recipes -->
        <span class="number">{{recipe.id}}</span> {{recipe.name}}
      </li>
    </ul>
    <h2>{{recipe.name}} ingredients</h2>
    <div><label>№: </label>{{recipe.id}}</div>
    <div>
      <label>title: </label>
      <input [(ngModel)]="recipe.name" placeholder="title">
    </div>
  `,
  styles: [`
    .selected {
      background-color: #CFD8DC !important;
      color: white;
    }
    .recipes {
      margin: 0 0 2em 0;
      list-style-type: none;
      padding: 0;
      width: 15em;
    }
    .recipes li {
      cursor: pointer;
      position: relative;
      left: 0;
      background-color: #EEE;
      margin: .5em;
      padding: .3em 0;
      height: 1.6em;
      border-radius: 4px;
    }
    .recipes li.selected:hover {
      background-color: #BBD8DC !important;
      color: white;
    }
    .recipes li:hover{ 
      color: #607D8B;
      background-color: #DDD;
      left: .1em;
    }
    .recipes .text {
      position: relative;
      top: -3px;
    }
    .recipes .number {
      display: inline-block;
      font-size: small;
      color: white;
      padding: 0.8em 0.7em 0 0.7em;
      background-color: #607D8B;
      line-height: 1em;
      position: relative;
      left: -1px;
      top: -4px;
      height: 1.8em;
      margin-right: .8em;
      border-radius: 4px 0 0 4px;
    }
  `]
  // templateUrl: './app.component.html',
 // styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Recipes';
  // recipe = 'Borsch';
  recipes = RECIPES;
  recipe: Recipe = {
    id: 1,
    name: 'Borsch'
  };
}
